﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

      //  private Text countText;
        private int count;

        public float moveSpeed;
        private float moveVelocity;
        public float jumpHeight;

        public Transform groundCheck;
        public float groundCheckRadius;
        public LayerMask whatIsGround;
        private bool grounded;

        private bool doubleJumped;

        private Animator anim;

        public Transform firePoint;
        public GameObject ninjaStar;

        public AudioSource jumpSound;
       
	// Use this for initialization
	void Start () 
    {
        anim = GetComponent<Animator>();
        count = 0;
        //SetCountText();
	}
    //Cheking the enviroment of the platform
    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

   
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
           other.gameObject.SetActive(false);
           count = count + 1;
          // SetCountText();
        }
        
    }

    /*
    void SetCountText()
    {
        countText.text = "Count :" + count.ToString();
    }
     */

	// Update is called once per frame
	void Update () 
    {
        // checking for double jump
        if (grounded)
            doubleJumped = false;
            anim.SetBool("Grounded",grounded);

        //Player control double jump
        if (Input.GetKeyDown(KeyCode.UpArrow) && grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
            jumpSound.Play();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && !doubleJumped && !grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
            doubleJumped = true;
            jumpSound.Play();
        }

        moveVelocity = 0f;

        // Player control left & rigth
        if (Input.GetKey(KeyCode.RightArrow))
        {
            //GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            moveVelocity = moveSpeed;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            moveVelocity = -moveSpeed;
        }

        //to put some friction in the wall and ground if the player is not jumping
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);

        //Removing the sticky walls
        anim.SetFloat("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x));

        // animation for right movement
        if (GetComponent<Rigidbody2D>().velocity.x > 0)
            transform.localScale = new Vector3(1f,1f,1f);
        else if(GetComponent<Rigidbody2D>().velocity.x < 0)
            transform.localScale = new Vector3(-1f, 1f, 1f);

        if (Input.GetKeyDown(KeyCode.Return))
        {
            Instantiate(ninjaStar, firePoint.position, firePoint.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Instantiate(ninjaStar, firePoint.position, firePoint.rotation);
        }

	}

}
