﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Question[] question;

    private static List<Question> unansweredQuestions;

    private static List<Question> answeredQuestions;

    private static int countfor = 10;

    public Text quizScore;

    public static int countScore;

    private Question currentQuestion;

    [SerializeField]

    private Text FactText;

    [SerializeField]

    private Text trueAnswerText;

    [SerializeField]

    private Text falseAnswerText;

    [SerializeField]

    private Animator animator;

    [SerializeField]
    private float timeBetweenQuestions = 1f;

    // Use this for initialization
	void Start () 
    {
        if(unansweredQuestions == null || unansweredQuestions.Count == 0)
        {
                countfor++;
                unansweredQuestions = question.ToList<Question>();

        }
        countfor++;
            SetCurrentQuestion();
            if (countfor == 10)
            {
                Debug.Log("done");
            }  
	}

    void SetScoreText()
    {

        quizScore.text = "Score : " + countScore.ToString() ;
    }

    void SetCurrentQuestion()
    {
        
        int randomQuestionIndex = Random.Range(0, unansweredQuestions.Count);
        currentQuestion = unansweredQuestions[randomQuestionIndex];

        FactText.text= currentQuestion.fact;    

        unansweredQuestions.RemoveAt(randomQuestionIndex);

        if (currentQuestion.isTrue)
        {
            
            trueAnswerText.text = "Correct";
            falseAnswerText.text = "Wrong";
          
        }
        else
        {
           
            trueAnswerText.text = "Wrong";
            falseAnswerText.text = "Correct";
        }

    }

    IEnumerator TransitionToNextQuestion()
    {
        unansweredQuestions.Remove(currentQuestion);

        yield return new WaitForSeconds(timeBetweenQuestions);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void UserSelectTrue()
    {
        
        animator.SetTrigger("True");
        if (currentQuestion.isTrue)
        {
            countScore = countScore + 2;
            SetScoreText();
            Debug.Log("Correct");
        }
        else
        {
            Debug.Log("Wrong");
        }
        
        StartCoroutine(TransitionToNextQuestion());
    }

    public void UserSelectFalse()
    {
        animator.SetTrigger("False");
        if (!currentQuestion.isTrue)
        {
            countScore = countScore + 2;
            SetScoreText();
           Debug.Log("Correct");
        }
        else
        {
           Debug.Log("Wrong");
        }
        StartCoroutine(TransitionToNextQuestion());
    }
	
	// Update is called once per frame
	void Update () 
    {
        SetScoreText();

        if (countfor == 10)
        {
            Debug.Log("done");
        }
        
	}
}
