﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MaterialUI
{
	public class CheckBoxToggler : MonoBehaviour, IPointerClickHandler
	{
		public Toggle theToggle;

		public void OnPointerClick (PointerEventData data)
		{
			if (theToggle.interactable)
				theToggle.isOn = !theToggle.isOn;
		}
	}
}