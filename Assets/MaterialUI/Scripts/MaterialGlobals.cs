﻿using UnityEngine;
using System.Collections;

namespace MaterialUI
{
    public class MaterialGlobals : MonoBehaviour
    {
        public static Vector4 shadowSpriteBorder;
        public static float uiScale;
    }
}
