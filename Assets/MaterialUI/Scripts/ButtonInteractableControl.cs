﻿using UnityEngine;
using UnityEngine.UI;

namespace MaterialUI
{
	[ExecuteInEditMode]
	[RequireComponent(typeof (CanvasGroup))]
	public class ButtonInteractableControl : MonoBehaviour
	{
		private CanvasGroup canvasGroup;
		private Button button;

		private bool lastInteractableState;

		[SerializeField] private CanvasGroup shadows;

		private void OnEnable()
		{
			canvasGroup = gameObject.GetComponent<CanvasGroup>();
			button = gameObject.GetComponent<Button>();
		}

		void Update()
		{
			if (lastInteractableState != button.interactable)
			{
				lastInteractableState = button.interactable;

				if (lastInteractableState)
				{
					canvasGroup.alpha = 1f;
					canvasGroup.blocksRaycasts = true;

					if (shadows)
						shadows.alpha = 1f;
				}
				else
				{
					canvasGroup.alpha = 0.5f;
					canvasGroup.blocksRaycasts = false;

					if (shadows)
						shadows.alpha = 0f;
				}
			}
		}
	}
}